class Prover(object):
  """ Fake prover implementation. """
  def send(self, command):
    return ""

from ProofWeb import ProofWeb
from coq_local import Coq_Local
from external import which

def local_which(program):
  """ Implementation of "which" for Python. """
  try:
    return which.which(program)
  except which.WhichError:
    print "Which error"
    return None
  
def get_prover(url = None, group = None, 
               path = None, args = None, shiftresp = None, include_command = False, include_command_min_time = 300):
  """ Factory that determines what prover to serve. 
  """
  if path:
    return Coq_Local(path if not args else (path + " " + args), shiftresp, include_command, include_command_min_time) 
  elif (url and group):
    return ProofWeb(url, group)
  elif local_which("coqtop"):
    coqtop = local_which("coqtop")
    return Coq_Local(coqtop if not args else (coqtop + " " + args), shiftresp, include_command, include_command_min_time)
  else:
    return Prover()
