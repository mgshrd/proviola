import difflib

class ThreeWayMatcher:
  def __init__(self, before, now, after, junk = lambda s: s): # junk : string -> string: transforming a string by _removing_ some characters, which should not be used during comparision
    before = before if before is not None else ""
    now = now if now is not None else ""
    after = after if after is not None else ""
    jb = junk(before)
    jn = junk(now)
    ja = junk(after)
    self.ps = difflib.SequenceMatcher(None, jb, jn).get_opcodes().__iter__()
    self.ns = difflib.SequenceMatcher(None, jn, ja).get_opcodes().__iter__()

    self.start = 0
    self.junkoffset = 0
    self.junktag = "equal"

    self.nend = 0
    self.ntag = "empty"

    self.pend = 0
    self.ptag = "empty"

    self.junkednow = jn
    self.now = now
    # validate junk contract
    chrat = lambda s, i: s[i:i+1]
    jo = 0
    i = 0
    while chrat(self.now, i+jo) != "" and chrat(self.junkednow, i) != "":
      while chrat(self.junkednow, i) != chrat(self.now, i+jo):
        jo = jo + 1
      i = i + 1
    if junk(self.now[i+jo:]) != "" or chrat(self.junkednow, i) != "":
#      jo = 0
#      i = 0
#      while chrat(self.now, i+jo) != "" and chrat(self.junkednow, i) != "":
#        while chrat(self.junkednow, i) != chrat(self.now, i+jo):
#          jo = jo + 1
#        i = i + 1
#        print "XXX '" + chrat(self.now, i+jo) + "' '" + chrat(self.junkednow, i) + "'"
      raise BaseException("Invalid junking of '" + self.now + "': '" + self.now[i+jo:] + "' '" + self.junkednow[i:] + "'")

  def __iter__(self):
    return self

  def _jnext(self):
    while self.ptag != "end" and self.start == self.pend:
      try:
        ptag, pi1, pi2, pj1, pj2 = self.ps.next()
        if ptag == "equal" or ptag == "delete":
          self.ptag = "equal"
        else:
          self.ptag = "change"
        assert self.start == pj1
        self.pend = pj2
      except StopIteration:
        self.ptag = "end"

    while self.ntag != "end" and self.start == self.nend:
      try:
        ntag, ni1, ni2, nj1, nj2 = self.ns.next()
        if ntag == "equal" or ntag == "insert":
          self.ntag = "equal"
        else:
          self.ntag = "change"
        assert self.start == ni1
        self.nend = ni2
      except StopIteration:
        self.ntag = "end"

    if self.ptag == "end" and self.ntag == "end":
      raise BaseException("Internal error")

    self.start += 1
    restag = "equal"
    if self.ptag == "change" and self.ntag == "change":
      restag = "both"
    elif self.ptag == "change":
      restag = "add"
    elif self.ntag == "change":
      restag = "remove"
    self.junktag = restag
    return (self.start + self.junkoffset, restag)

  def next(self):
    chrat = lambda s, i: s[i:i+1]
    if chrat(self.junkednow, self.start) != chrat(self.now, self.start + self.junkoffset):
      if chrat(self.now, self.start + self.junkoffset) == "":
        raise StopIteration
      else:
        self.junkoffset = self.junkoffset + 1
        return ((self.start + self.junkoffset, self.junktag))
    elif chrat(self.junkednow, self.start) == "":
      raise StopIteration
    else:
      return self._jnext()

  def collect(self):
    res = []

    tagaccu = ""
    lasttag = ""

    for i, tag in self:
      if lasttag != tag:
        if lasttag != "":
          res.append((lasttag, tagaccu))
          tagaccu = ""
        lasttag = tag
      tagaccu = tagaccu + self.now[i-1:i]
    if tagaccu != "":
      res.append((lasttag, tagaccu))

    verifres = ""
    for _, x in res:
      verifres = verifres + x
    assert self.now == verifres

    return res
