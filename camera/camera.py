#!/usr/bin/env python
#
# Author: Carst Tankink carst 'at' cs 'dot' ru 'dot' nl
# Copyright: Radboud University Nijmegen
#
# This file is part of the Proof Camera.
#
# Proof Camera is free software: you can redistribute it and/or modify
  # it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Proof Camera is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with Proof Camera.  If not, see <http://www.gnu.org/licenses/>.


from Reader_Factory import get_reader

import sys
import os
from os.path import splitext
import logging
import codecs
from argparse import ArgumentParser
from Prover import get_prover
import re

def setupParser():
  """ Setup a command line parser """

  usage = """ Creates a movie from foo.v, storing in bar.flm, if provided,
              or in foo.flm."""


  parser = ArgumentParser(description = usage)
  parser.add_argument("-u", "--user", 
                    action="store", dest="user",
                    default="nobody",
                    help="Username for ProofWeb (default: %(default)s)")

  parser.add_argument("-g", "--group", 
                    action="store", dest="group",
                    default="nogroup",
                    help="Groupname for ProofWeb user (default: %(default)s)")

  parser.add_argument("-p", "--password",
                    action="store", dest="pswd",
                    default="anon",
                    help="Password for user")
  
  parser.add_argument("--coqtop", 
                      action = "store", dest = "coqtop",
                      default=None,
                      help = "Location of coqtop executable."
                      )
  
  parser.add_argument("--coq-project", 
                      action = "store", dest = "coqproject",
                      default=None,
                      help = "_CoqProject file to convert to command line args to coqtop."
                      )

  parser.add_argument("--shift-response", 
                      action = "store_true", dest = "shiftresp",
                      default=False,
                      help = "Show response to previous tactic. Show rationale instead of result."
                      )
  
  parser.add_argument("--include-command", 
                      action = "store_true", dest = "include_command",
                      default=False,
                      help = "Include the command in the response <div>. Alleviates the need for the back and forth between the script and the state."
                      )

  parser.add_argument("--include-command-min-time-ms",
                    action="store", dest="include_command_min_time",
                    type=float, default="300",
                    help="Include how long a command executed, if above this millisecond value. 300ms by default."
                    )
  
  parser.add_argument("--response-diff", 
                      action = "store_true", dest = "response_diff",
                      default=False,
                      help = "Highlight changes to the previous response (or the next response in case --shift-response is also specified.)"
                      )
  
  parser.add_argument("--timeout", "-t",
                      action = "store", dest = "timeout",
                      type = float, default = 1,
                      help = """How long to wait for responses by coqtop. 
                      (In seconds, floating point).
                      """)
    
  parser.add_argument("--service-url",
                    action="store", dest="service",
                    default=None,
                    help="URL for web service to talk to prover")
  
  parser.add_argument("--prover",
                    action="store", dest="prover",
                    default="coq",
                    help="Prover to use (default: %(default)s).")
  
  parser.add_argument("--stylesheet",
                    action="store", dest="stylesheet",
                    default="proviola.xsl",
                    help="URI at which the XSL stylesheet can be found\
                      (default: %(default)s)")
  
  parser.add_argument("--version",
                      action = "store_true", dest = "showversion",
                      default=False,
                      help = "Display the version of proviola."
                      )
  
  parser.add_argument("script", action="store", 
                      help="Script from which to make a movie")

  parser.add_argument("movie", action="store", nargs="?", default=None,
                      help="Movie file in which to store the constructed movie")
                    
  return parser

def main(argv = None):
  """ Main method: 
      - sets up logging information, 
      - parses the command line (optionally provided as a list),
      - creates a film out of the given script.
      - Writes the film to disk.

      Arguments:
      - Argv: Arguments passed to the options parser. 
  """

  logging.basicConfig(stream=sys.stderr, level=logging.INFO)

  parser = setupParser()
  options = parser.parse_args(argv)

  if options.showversion:
    print("unspecified-beque")
    sys.exit()

  proofScript = options.script
  
  logging.debug("Processing: %s"%proofScript)

  movie = make_film(filename=proofScript, 
                    coqtop = options.coqtop,
                    coqproject = options.coqproject,
                    pwurl = options.service, 
                    group = options.group,
                    shiftresp = options.shiftresp,
                    include_command = options.include_command,
                    include_command_min_time = options.include_command_min_time)

  if options.movie:
    filmName = options.movie
  else:
    filmName = splitext(proofScript)[0] + ".flm" 

  directory = os.path.dirname(filmName)

  if len(directory) > 0 and not os.path.exists(directory):
    os.makedirs(directory)
  
  movie.set_response_diff(options.response_diff)
  movie.toFile(filmName, options.stylesheet)

def parse_coqproject(path):
  if path is None:
    return None

  comment = re.compile("#.*")
  with open(path) as f:
    content = [comment.sub("", line.strip()) for line in f]
    content = filter(lambda x: x != "", content)
    return " ".join(content)

def make_film(filename, pwurl = None, group = "nogroup",
                        coqtop = None, coqproject = None, shiftresp = None, include_command = False, include_command_min_time = 300):
  """Main method of the program/script: This creates a flattened 'film' for
   the given file filename.

    Arguments:
    - filename: The filename of the script to read.

    Keyword arguments:
    - pwurl: The URL to the server generating proof states.
    - group: The group used to log in.
  """ 

  extension = splitext(filename)[1] 
  reader = get_reader(extension = extension)
  with codecs.open(filename, 'r', encoding="utf-8") as f:
    reader.add_code(f.read())
  
  prover = get_prover(path = coqtop, args = parse_coqproject(coqproject), url = pwurl, group = group, shiftresp = shiftresp, include_command = include_command, include_command_min_time = include_command_min_time)

  return reader.make_frames(prover = prover)
  

if __name__ == "__main__":
  sys.exit(main())
