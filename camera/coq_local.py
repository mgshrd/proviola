""" Implements protocol for talking to local Coq installation.
"""
import shlex
import time
import signal
import pexpect
import re
import logging

class Coq_Local(object):
  def __init__(self, coqtop="/usr/bin/coqtop", shiftresp = False, include_command = False, include_command_min_time = 300):
    """ Open a Coq process. 
      - coqtop: Location of coqtop executable.
      - timeout: How long to wait for coqtop to print to stdout. 
    """
    self._coqtop = None
    self._coqtop = pexpect.spawn(coqtop + ' -emacs')
    self._coqtop.setecho(False)
    self._coqtop.delaybeforesend = None
    self._shiftresp = shiftresp
    self._include_command = include_command
    self._include_command_min_time = include_command_min_time
    self.lastresp = ""
    self.multipromptre = re.compile("\\A\\s*[-+*{}]", re.MULTILINE)
    self.pre = re.compile("<prompt>[^\\n]*</prompt>")
    
    # Clear Coq greeting.
    data = self._read_coq()
    if not data:
      print "Could not manage coq."

  def _read_coq(self, minprompts = 1, accu = ""):
    """ Read data from Coqtop. Read stdout after the  """
    self._coqtop.expect("..*")
    frag = self._coqtop.after
    fullmsg = accu + frag
    prompts = len(self.pre.findall(fullmsg))
    if prompts >= minprompts:
        res = self._clean(fullmsg)
    else:
        res = self._read_coq(minprompts, fullmsg)
    return res

  def _clean(self, string):
    """ Clean a string. """
    string = self.pre.sub("", string)
    string = string.lstrip()
    string = string.replace("\r", "")
    string = string.lstrip()
    string = string.rstrip()
    return "".join([c for c in string if ord(c) != 253])
  
  def __del__(self):
    """ Clean up: stop Coq process. """
    if self._coqtop:
      self._coqtop.close()
    
  def send(self, command):
    """ Send data to Coqtop, returning the result. """
    then = time.time() # .monotonic() is not available in 2.7
    self._coqtop.sendline(command)
    rest = command
    promptc = 0
    while self.multipromptre.search(rest) != None:
        rest = self.multipromptre.sub("", rest) # eat up all the groupings
        promptc = promptc + 1
    if re.compile("[^\\s]", re.MULTILINE).search(rest):
        promptc = promptc + 1 # if anything left over, it's assumed to be a tactic
    #print ("sending for %d prompts '" % promptc) + command + "'"
    try:
        res = self._read_coq(promptc)
    except pexpect.exceptions.TIMEOUT:
        logging.error("Command '" + command + ("' timed out while waiting for %d prompts." % promptc))
        raise pexpect.exceptions.TIMEOUT(command)

    dt = (time.time() - then) * 1000
    if self._shiftresp:
        tmp = res
        res = self.lastresp
        self.lastresp = tmp

    if self._include_command:
        cc = "\n".join(filter(lambda x: x != "", command.strip().splitlines()))
        if self._shiftresp:
            res = res + "\n\nIssuing: '" + cc + "' next."
            if self._include_command_min_time <= dt:
              res = res + "\nIt will take " + ("%d" % dt) + "ms"
        else:
            tmp = "Result of: '" + cc + "'."
            if self._include_command_min_time <= dt:
              tmp = tmp + "\nTook " + ("%d" % dt) + "ms"
            res = tmp + "\n\n" + res

    return res
   
  def interrupt(self):
    self._coqtop.kill(signal.SIGINT)

  def close(self):
    if self._coqtop:
      self._coqtop.close()
