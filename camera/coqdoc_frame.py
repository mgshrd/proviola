from Frame import Frame
from functools import partial
from lxml import etree
import re
import ThreeWayMatcher

TAG_COQDOC = "command-coqdoc"


class Coqdoc_Frame(Frame):
  """ A coqdoc_frame is a frame with an added command-coqdoc section. """
  def __init__(self, id = -1, command = None, 
                             command_cd = None, 
                             response = None):
    Frame.__init__(self, id = id, command = command, response = response)
    self._command_coqdoc = command_cd if command_cd is not None else []

  def get_coqdoc_command(self):
    """ Getter for self._command_coqdoc. """
    if not self._command_coqdoc:
      return ""
    
    return "".join([self._tostring(part) for part in self._command_coqdoc])

  def append_to_markup(self, element):
    """ Append element to markup. """
    self._command_coqdoc.append(element)

  def _tostring(self, el):
    """ Converts html.tostring(el) if el is an element, or el itself. """
    try:
      return etree.tostring(el)
    except TypeError:
      return el

  def get_markup_command(self):
    """ Getter for marked up command. """
    return self.get_coqdoc_command()

  def fromxml(self, element):
    """ Instantiate the data using the given element.
    """ 
    Frame.fromxml(self, element)    
    
    el = element.find(TAG_COQDOC)
    
    if el.text:
      self._command_coqdoc.append(el.text)
    
    map(self._command_coqdoc.append, el)

  
  def _append_etree(self, root, element):
    """ Append element to root. If element is text, it will overwrite root.text.
    """
    try:
      root.append(element)
    except TypeError:
      root.text = element

  def _get_goal(self, state):
    state = state if state is not None else u""
    pre = None
    goal = u""
    post = u""
    sm = "pre"
    for line in state.splitlines():
      if sm == "pre" or sm == "pre2" or sm == "pre3":
        if re.search("^[0-9]+ (focused )?subgoal", line) is not None:
          sm = "pre2"
          if pre is None:
            pre = line
          else:
            pre = pre + "\n" + line
        elif sm == "pre2" and re.search("\\A\\s*\\Z", line) is not None:
          sm = "pre3"
          pre = pre + "\n" + line
        elif sm == "pre3" and re.search("^\\s", line) is not None:
          sm = "goal"
          pre = pre + "\n"
          goal = line
        else:
          if pre is None:
            pre = line
          else:
            pre = pre + "\n" + line
      elif sm == "goal":
        if re.search("\\A\\s*\\Z", line) is not None or re.search("^[^\\s]", line) is not None:
          sm = "post"
          post = "\n" + line
          goal = goal
        else:
          if goal != "":
            goal = goal + "\n"
          goal = goal + line
      elif sm == "post":
          post = post + "\n" + line
      else:
          raise BaseException("unknown parse state: " + sm)
      
    return (pre, goal, post)

  def _flatten(self, s):
      s = s.strip()
      s = re.sub("\n   +", " ", s) # remove line breaks within hypotheses&the goal
      s = re.sub(" +", " ", s) # squash spaces
      s = re.sub("\n ", "\n", s) # remove leading spaces
      return s

  def get_diff(self, resp, prev_resp = None, next_resp = None):
    _, prev_goal, _ = self._get_goal(prev_resp) if prev_resp is not None else ("", u"", "")
    _, next_goal, _ = self._get_goal(next_resp) if next_resp is not None else ("", u"", "")

    preamble, goal, footer = self._get_goal(resp)

    res = []

    if preamble is not None and preamble != u"":
      res.append(("equal", preamble))

    hypsgoalsep = "  ============================"
    if re.search(hypsgoalsep, prev_goal) is None:
      prev_goal = goal
    if re.search(hypsgoalsep, goal) is None:
      res.append(("equal", goal))
      res.append(("equal", footer))
      return res
    if re.search(hypsgoalsep, next_goal) is None:
      next_goal = goal

    hypsgoalsplit = lambda s: re.split("(" + hypsgoalsep + ")", s)
    ph, _, pg = hypsgoalsplit(prev_goal)
    h, _, g = hypsgoalsplit(goal)
    nh, _, ng = hypsgoalsplit(next_goal)
    diff3 = lambda p, c, n: ThreeWayMatcher.ThreeWayMatcher(p, c, n, lambda s: self._flatten(s)).collect()
    res = res + diff3(ph, h, nh)
    res.append(("equal", hypsgoalsep))
    res = res + diff3(pg, g, ng)

    if footer is not None and footer != u"":
      res.append(("equal", footer))

    return res


  def toxml(self, prev_frame = None, next_frame = None, showdiff = False):
    """ Convert this frame to XML, using etree. """
    frame_xml = Frame.toxml(self, prev_frame, next_frame, showdiff)
    coqdoc = etree.SubElement(frame_xml, TAG_COQDOC)
   
    map(partial(self._append_etree, coqdoc), self._command_coqdoc)
    
    return frame_xml

  def __str__(self):
    return """
Frame(id       = {id},
      command  = {command},
      coqdoc   = {command_cd}
      response = {response})""".format(id = self._id, 
                                       command = self._command,
                                       command_cd = self._command_coqdoc,
                                       response = self._response)
