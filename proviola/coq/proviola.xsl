<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet version="1.0"  
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:template match="/">
  <html>
  <head>
    <link href="coqdoc.css" type="text/css" rel="stylesheet" />      
    <style>
    	div.output { position: fixed; bottom: 0; right: 0; display: none; background-color: white; border: 1px solid black; }
    	div.output:hover { display: block; }
    	span.command:hover { background-color: lightgray; }
    	span.command:hover + div.output { display: block; }
    	span.command:focus { background-color: lightgray; }
    	span.command:focus + div.output { display: block; }
	span.resp_add { background-color: lightgreen; }
	span.resp_remove { background-color: lightpink; }
	span.resp_both { background-color: lightpink; color: green; }
    </style>
    
  </head>

  <body>
    <xsl:for-each select="movie/scenes">
        <xsl:apply-templates />
      </xsl:for-each>
  </body>
  </html>

</xsl:template>

<xsl:template match="frame">
  <xsl:param name="scene-type"/>
 
  <xsl:choose>
  <xsl:when test="$scene-type = 'doc'">
    <xsl:copy-of select = "command-coqdoc/node()"/>
  </xsl:when>
  <xsl:otherwise>
    <span class="command" tabindex="1"><xsl:copy-of select = "command-coqdoc/node()"/></span>
    <div class="output"><pre><xsl:copy-of select="response/node()"/></pre></div>
  </xsl:otherwise>
  </xsl:choose>

</xsl:template>

<xsl:template match="frame-reference">
  <xsl:param name="scene-type">doc</xsl:param>
  <xsl:variable name = "ref">
    <xsl:value-of select="@framenumber" />
  </xsl:variable>
  <xsl:apply-templates select="/movie/film/frame[@framenumber = $ref]">
    <xsl:with-param name="scene-type" select="$scene-type"/>
  </xsl:apply-templates>
</xsl:template>

<xsl:template match="scene">
  <xsl:param name="scene-type">doc</xsl:param>
  <div>
    <xsl:for-each select="@*">
    </xsl:for-each>
    
    <xsl:apply-templates>
      <xsl:with-param name="scene-type">
        <xsl:value-of select="@class"/>
       </xsl:with-param>
     </xsl:apply-templates>
  </div> 
</xsl:template>
</xsl:stylesheet>
